
ELF.own.Stats = (function() 
{
	
	function Stats()
	{
		readHeroJson();
		readOrcJson();
	}

	function showHeroAttributes() 
	{
	   'use strict';
	   $("#hero_login").html(ELF.run.hero.login);
	   $("#hero_type").html(ELF.run.hero.type);
	   $("#hero_moves").html(ELF.run.hero.moves);
	   $("#hero_experience").html(ELF.run.hero.experience);    
	   $("#hero_health").html(ELF.run.hero.health);   
	   $("#hero_strength").html(ELF.run.hero.strength);   
	}

	function showOrcAttributes()
	{
		 'use strict';
		 $("#orc_login").html(ELF.run.orc.login);
		 $("#orc_type").html(ELF.run.orc.type);
		 $("#orc_moves").html(ELF.run.orc.moves);
		 $("#orc_experience").html(ELF.run.orc.experience);    
		 $("#orc_health").html(ELF.run.orc.health);   
		 $("#orc_strength").html(ELF.run.orc.strength);  
	}
	
	// Convert the hero and orc attributes from strings to integers,
	// so we can use them for calculations.
	function convertToInts(hero)
	{
		hero.x = parseInt(hero.x);
		hero.y = parseInt(hero.y);
		hero.moves = parseInt(hero.moves);
		hero.experience = parseInt(hero.experience);
		hero.health = parseInt(hero.health);
		hero.strength = parseInt(hero.strength);
		
		return hero;
	}

	function readHeroJson() 
	{
		$.getJSON('/readHero', function( charAttributes ) 
		{
			ELF.run.hero = convertToInts(charAttributes);
			//showHeroAttributes(); 
		})
		.success(function() { showDebug('success'); })
		.error(function(jqXHR, textStatus, errorThrown) 
		{ 
			alert("error calling JSON. Try JSONLint or JSLint: " + textStatus); 
		})
		.complete(function() { console.log("csc: completed call to get index.json"); });
	}
	
	function readOrcJson() 
	{
		$.getJSON('/readOrc', function( charAttributes ) 
		{
			ELF.run.orc = convertToInts(charAttributes);
			//showHeroAttributes(); 
		})
		.success(function() { showDebug('success'); })
		.error(function(jqXHR, textStatus, errorThrown) 
		{ 
			alert("error calling JSON. Try JSONLint or JSLint: " + textStatus); 
		})
		.complete(function() { console.log("csc: completed call to get index.json"); });
	}

	Stats.prototype.writeJson = function() 
	{
		$.ajax({
			type: 'GET',
			url: '/write',
			dataType: 'json',
			data: ELF.run.hero, 
			success: function(data) {
				showDebug(data.result);
			},
			error: showError      
		});
		
		$.ajax({
			type: 'GET',
			url: '/write',
			dataType: 'json',
			data: ELF.run.orc, 
			success: function(data) {
				showDebug(data.result);
			},
			error: showError      
		});
	}

	var showError = function(request, ajaxOptions, thrownError) 
	{
		showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
		showDebug(request.status);
		showDebug(request.statusText);
		showDebug(request.getAllResponseHeaders());
		showDebug(request.responseText);
	}
	
	var showDebug = function(textToDisplay)
	{
		$("#debug").append('<li>' + textToDisplay + '</li>');
	}

	var ShowMessage = function(textToDisplay)
	{
		$("#message").append('<li>' + textToDisplay + '</li>');
	}

	Stats.prototype.InitStats = function()
	{
		ELF.run.hero = 
		{ 
			login: $('#hero_login').html(), 
			type: $('#hero_type').html(), 
			moves: parseInt($('#hero_moves').html()), 
			experience: parseInt($('#hero_experience').html()),
			health: parseInt($('#hero_health').html()),
			strength: parseInt($('#hero_strength').html())
		};
		
		ELF.run.orc = 
		{
			login: $('#orc_login').html(), 
			type: $('#orc_type').html(), 
			moves: parseInt($('#orc_moves').html()), 
			experience: parseInt($('#orc_experience').html()),
			health: parseInt($('#orc_health').html()),
			strength: parseInt($('#orc_strength').html())
		};
	}



	Stats.prototype.CalcStrike = function()
	{
		$("#message").html('');
		
		var randomNumber = Math.floor(Math.random() * 100) + 1;
		var bonus = ELF.run.hero.experience + ELF.run.hero.strength;
		
		ShowMessage("randomNumber = " + randomNumber);
		ShowMessage("bonus = " + bonus);
		if( randomNumber + bonus > 50 )
		{
			ShowMessage("Hit!")
			ELF.run.orc.health = ELF.run.orc.health - 1;
			
			// hero has 25% chance to gain 1 experience point.
			var randomNumber2 = Math.floor(Math.random() * 100) + 1;
			ShowMessage("randomNumber2 = " + randomNumber2);
			if( randomNumber2 < 25 )
			{
				ELF.run.hero.experience += 1;
				ShowMessage("Experience Gained!");
			}
		}
		
		showHeroAttributes();
		showOrcAttributes();
	}
	
	Stats.prototype.UnitTests = function()
	{
	
		// Unit Test Cases
		module("Stats Unit Test");
		test("Stats - Hero attributes loaded", function()
		{
			var actual = ELF.run.hero;
			var expected = null;
			notEqual(actual, expected, 'Is the Hero attributes loaded' + ELF.run.hero);
			
		});
	}


	return Stats;
})();

