
ELF.own.Grid = (function() 
{

	
	// Private variables (in closure)
	var context = null;
	var rectSize = 25;
	var width = -1;
	var height = -1;
	var count = 0;
	var score = 0;

	
    var npc = [			
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                         	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                         ];
              
          	
      var grid = 
          				[
          				[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
          				[1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
          				[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          				[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          				[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          				[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          				[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          				[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          				[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	            [0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	            [0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0],
          				[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
          	            ];
	
	goal_x = 0;
	goal_y = 19;

	

	var imagesPalette = new Image();
	var imagesFilename = "public/images.gif";
	
	var npcImagesPalette = new Image();
	var npcImagesFilename = "public/npc.gif";
	
	var terrainImagesPalette = new Image();
	var terrainImagesFilename = "public/terrainAtlas.gif";


	// Constructor has Caps, must be called with new, returned from App
	function Grid() 
	{
		module("Grid Unit Test");
		test("Grid - Hero object exists", function()
				{
					var actual = ELF.run.hero;
					var expected = null;
					notEqual(actual, expected, 'Is the Hero attributes loaded' + ELF.run.hero);
				});
		
		test("Grid - Hero X > 0", function()
				{

					ok(ELF.run.hero.x >= 0, 'Is the Hero attributes loaded. x = ' + ELF.run.hero.x);
				});
		
		ELF.run.keyboardinput = new ELF.own.KeyboardInput(this);
		window.addEventListener('keydown', ELF.run.keyboardinput.doKeyDown, true);
		
		context = getCanvas();
		makeImageData();
		
		
	}
	
	Grid.prototype.getGrid = function()
	{
		return grid;
	}

	var getCanvas = function() 
	{
		canvas = document.getElementById('mainCanvas');
		if (canvas !== null) {
			width = canvas.width;
			height = canvas.height;
			context = canvas.getContext('2d');
			setInterval(draw, 50
					);
			return context;
		} else {
			$("#debugs").css({
				backgroundColor : "blue",
				color: "yellow"
			});
			$("#debugs").html("Could not retrieve Canvas");
			return null;
		}
	}
	
	var draw = function() 
	{
		drawBoard();
	}
	
	var drawDarkImage = di =  function(x,y)
	{
		context.drawImage(imagesPalette, 0, rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
	
	}
	
	var drawLightImage = li = function(x,y)
	{
		context.drawImage(imagesPalette, 0, 0, rectSize, rectSize, x*rectSize,y*rectSize, rectSize, rectSize);
	}
	
	var drawCharacterImage = ci = function(x,y)
	{
		context.drawImage(imagesPalette, rectSize, 0, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawGoalImage = gi = function(x,y)
	{
		context.drawImage(imagesPalette, rectSize, rectSize, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawNPC1 = function(x,y)
	{
		context.drawImage(npcImagesPalette, 0, rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawNPC2 = function(x,y)
	{
		context.drawImage(npcImagesPalette, 0, 2*rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawNPC3 = function(x,y)
	{
		context.drawImage(npcImagesPalette, rectSize, 2*rectSize, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawTree = function(x,y)
	{
		//context.drawImage(terrainImagesPalette, rectSize, 1024-rectSize, 1024-rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
		context.drawImage(npcImagesPalette, rectSize, rectSize, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	

	

	var drawBoard = function()
	{		
		
		// Draw GRID
		for (var i=0; i<grid.length; i++)
			for( var j=0; j<grid[i].length; j++)
				if(grid[i][j] == 0 )
					drawDarkImage(i, j);
				else
					drawLightImage(i, j);
//				grid[i][j](i, j);

		// Draw NPCs
		for (var i=0; i<npc.length; i++)
			for( var j=0; j<npc[i].length; j++)
				if(npc[i][j] == 1 )
					drawNPC1(i, j);
				else if(npc[i][j] == 2 )
					drawNPC2(i, j);
				else if(npc[i][j] == 3 )
					drawNPC3(i, j);
		

		
		drawCharacterImage(ELF.run.hero.x,ELF.run.hero.y);
		drawGoalImage(goal_x, goal_y);
		drawTree(10,11);
	}
	
	
	var updateStats = function()
	{
		character.moves++;
		$("#hero_moves").html(character.moves);
		
		var randomNumber = Math.floor(Math.random() * 100) + 1;

		if( character.moves % 10 == 0 && randomNumber < 15 )
		{
			character.strength++;
			$("#hero_strength").html(character.strength);
		}
		
		if( character.moves % 25 == 0 )
		{
			character.experience++;
			$("#hero_experience").html(character.experience);
		}
		
	}
	
	
	var makeImageData = function() 
	{
		loadImage(imagesFilename, function() 
				{
			drawBoard();
		});
		
		npcImagesPalette.src = npcImagesFilename;
		terrainImagesPalette.src = terrainImagesFilename;

	}
	
	var loadImage = function( filename, callback )
	{
		imagesPalette.onload = function() 
			{ 
				callback(); 
			}; 
			imagesPalette.src = filename;

	}
	
	
	Grid.prototype.UnitTests = function()
	{
		// Unit Test Cases
		module("Grid Unit Test");
		test("Grid - Grid Size", function()
		{
			var actual = grid.length;
			var expected = 20;
			equal(actual, expected, 'Is the size of the Grid 20');
			
		});
	}

  
	
        //$("#movecount").html("movecount: "+ character.moves);
        
      /*  if( character_x == goal_x && character_y == goal_y )
        {
        	$("#message").html("You Won!!!!!!!");
        	$("#message").css("color", "red");
        }
    }*/

    

   
    


	        
	return Grid;
})();


