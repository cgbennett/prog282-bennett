var express = require('express');
var app = express();
var fs = require('fs');
var openid = require('openid');
var querystring = require('querystring');
var handlebars = require('handlebars');

var port = process.env.PORT || 30025;
var url = require('url');
var querystring = require('querystring');
var relyingParty = new openid.RelyingParty('http://54.243.217.141:30025/go', // Verification
		// URL
		// (yours)
		null, // Realm (optional, specifies realm for OpenID authentication)
		false, // Use stateless verification
		false, // Strict mode
		[]);

// We need this here to make POST call work
app.use(express.bodyParser());

app.get('/', function(req, res) 
{
	// var html = fs.readFileSync('public/index.html');
	// res.writeHeader(200, {"Content-Type": "text/html"});
	// res.end(html);

	// Deliver an OpenID form on all other URLs
	res.writeHead(200);
	res
			.end('<!DOCTYPE html><html><body>'
					+ '<form method="get" action="/authenticate">'
					+ '<p>Login using OpenID</p>'
					+ '<input name="openid_identifier" />'
					+ '<input type="submit" value="Login" />'
					+ '</form></body></html>');
});

app.get('/go', function(request, response) 
{
	'use strict';
	console.log('Go called');
	try 
	{
		var herotable = String(fs.readFileSync('public/HeroTable.html'));
		var orctable = String(fs.readFileSync('public/OrcTable.html'));
		var grid = String(fs.readFileSync('public/Grid.html'));
		var html = String(fs.readFileSync('public/GridGame.html'));
		
		var context = 
			{
				HeroTable: herotable,
				OrcTable: orctable,
				Grid: grid
			}
		
		console.log(context);
		var result = replaceHtml( context, html);
		
		response.writeHeader(200, 
			{
			"Content-Type" : "text/html"
			});
		response.end(result);
	} 
	catch (error) 
	{
		console.log(error);
	}

});

app.get('/launchSecondHtmlFile', function(request, response) 
{
	console.log('launchSecondHtmlFile called');
	try {
		var html = fs.readFileSync('Public/SecondFile.html');
		response.writeHeader(200, 
				{
				"Content-Type" : "text/html"
				});
			response.end(html);
	} 
	catch (error) 
	{
		console.log(error);
	}

});


app.get('/UnitTestCases', function(request, response) 
		{

			'use strict';
			
			console.log('UnitTestCases called');
			try {
				var grid = String(fs.readFileSync('public/Grid.html'));
				var html = String(fs.readFileSync('public/UnitTestCases.html'));
				
				var context = 
					{
						Grid: grid
					}
				
				var result = replaceHtml( context, html);
				
				
				response.writeHeader(200, 
						{
					"Content-Type" : "text/html"
				});
				response.end(result);
			} catch (error) {
				console.log(error);
			}

		});


app.get('/GridGame', function(request, response) 
{
	'use strict';
	console.log('GridGame called');
	try 
	{
		var herotable = String(fs.readFileSync('public/HeroTable.html'));
		var orctable = String(fs.readFileSync('public/OrcTable.html'));
		var grid = String(fs.readFileSync('public/Grid.html'));
		var html = String(fs.readFileSync('public/GridGame.html'));
		
		var context = 
			{
				HeroTable: herotable,
				OrcTable: orctable,
				Grid: grid
			}
		
		console.log(context);
		var result = replaceHtml( context, html);
		
		response.writeHeader(200, 
			{
			"Content-Type" : "text/html"
			});
		response.end(result);
	} 
	catch (error) 
	{
		console.log(error);
	}

});


app.get('/TestScoring', function(request, response) 
{
	'use strict';
	console.log('TestScoring called');
	try {
		var herotable = String(fs.readFileSync('public/HeroTable.html'));
		var orctable = String(fs.readFileSync('public/OrcTable.html'));
		var html = String(fs.readFileSync('public/TestScoring.html'));
		
		var context = 
			{
				HeroTable: herotable,
				OrcTable: orctable
			}
		
		console.log(context);
		console.log('got here2');
		var result = replaceHtml( context, html);
		
		
		response.writeHeader(200, 
				{
					"Content-Type" : "text/html"
				});
		response.end(result);
	} 
	catch (error) 
	{
		console.log(error);
	}

});


app.get('/TestGrid', function(request, response) 
{
	'use strict';
	console.log('TestGrid called');
	try {
		var grid = String(fs.readFileSync('public/Grid.html'));
		var html = String(fs.readFileSync('public/TestGrid.html'));
		
		var context = 
			{
				Grid: grid
			}
		
		var result = replaceHtml( context, html);
		
		
		response.writeHeader(200, 
				{
			"Content-Type" : "text/html"
		});
		response.end(result);
	} catch (error) {
		console.log(error);
	}

});

app.get('/readHero', function(request, response) {
	console.log('Read called.');
	var obj;

	function readData(err, data) {
		if (err)
			throw err;
		obj = JSON.parse(data);
		response.send(obj);
	}

	// Asynch call
	fs.readFile('SavedGames/hero.json', 'utf8', readData);
});

app.get('/readOrc', function(request, response) {
	console.log('Read called.');
	var obj;

	function readData(err, data) {
		if (err)
			throw err;
		obj = JSON.parse(data);
		response.send(obj);
	}

	// Asynch call
	fs.readFile('SavedGames/orc.json', 'utf8', readData);
});

app.get('/write', function(request, response) {
	console.log('gothere!!!!!')
	//console.log('Write called: ' + request.query);
	var person = request.query;
	var personString = JSON.stringify(person, null, 4);
	console.log('character object: ' + personString);
	
	if( person.type == 'hero')
	{
		fs.writeFile("SavedGames/hero.json", personString, 'utf8', function(err, data) {
			if (err)
				throw err;
			console.log('It\'s saved!');
		});
	}
	else if( person.type == 'orc')
	{
		fs.writeFile("SavedGames/orc.json", personString, 'utf8', function(err, data) {
			if (err)
				throw err;
			console.log('It\'s saved!');
		});
	}
	else
	{
		response.send('{"result":"failed"}');
		return;
	}
	

	response.send('{"result":"success"}');
});

app.get('/authenticate', function(req, res) {
	console.log("Authenticate called");
	console.log(req.query);
	// User supplied identifier
	var query = querystring.parse(req.query);
	console.log("Parsed Query: " + query);
	var identifier = req.query.openid_identifier;
	console.log("identifier: " + identifier);

	// Resolve identifier, associate, and build authentication URL
	relyingParty.authenticate(identifier, false, function(error, authUrl) {
		if (error) {
			res.writeHead(200);
			res.end('Authentication failed: ' + error.message);
		} else if (!authUrl) {
			res.writeHead(200);
			res.end('Authentication failed');
		} else {
			res.writeHead(302, {
				Location : authUrl
			});
			res.end();
		}
	});
});


function replaceHtml( context, html) {
	// This code uses Handlebars to put 'context'
	// into the 'html' variable text.
    'use strict';
   
    var template=handlebars.compile(html);
    var result = template(context);
 
    return result;
}


app.use("/public", express.static(__dirname + '/public'));

app.listen(port);
console.log('Listening on port :' + port);
