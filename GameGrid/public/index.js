
	
var character = null;
var orc = null;

var Game = (function() {

	// Private variables (in closure)
	var context = null;
	var rectSize = 25;
	var width = -1;
	var height = -1;
	var count = 0;
	var score = 0;
	//var movecount = 0;

	
	var character_x = 0;
	var character_y = 0;
	
	var GRID_SIZE_X = 12;
	var GRID_SIZE_Y = 12;
	
	goal_x = 0;
	goal_y = 11;
	
	//var heroChar = null;
	//var orcChar = null;
	

	var imagesPalette = new Image();
	var imagesFilename = "public/images.gif";
	//var imagesFilename = "images.gif";

	// Constructor has Caps, must be called with new, returned from App
	function Game() {
		context = getCanvas();
		makeImageData();

	}

	var getCanvas = function() {
		canvas = document.getElementById('mainCanvas');
		if (canvas !== null) {
			width = canvas.width;
			height = canvas.height;
			context = canvas.getContext('2d');
			setInterval(draw, 50
					);
			return context;
		} else {
			$("#debugs").css({
				backgroundColor : "blue",
				color: "yellow"
			});
			$("#debugs").html("Could not retrieve Canvas");
			return null;
		}
	}
	var draw = function() {
		drawBoard();
	}
	
	var drawDarkImage = di =  function(x,y)
	{
		context.drawImage(imagesPalette, 0, rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
	
	}
	
	var drawLightImage = li = function(x,y)
	{
		context.drawImage(imagesPalette, 0, 0, rectSize, rectSize, x*rectSize,y*rectSize, rectSize, rectSize);
	}
	
	var drawCharacterImage = ci = function(x,y)
	{
		context.drawImage(imagesPalette, rectSize, 0, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	
	var drawGoalImage = gi = function(x,y)
	{
		context.drawImage(imagesPalette, rectSize, rectSize, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	

	var drawBoard = function()
	{		
		for (var i=0; i<GRID_SIZE_X; i++)
			for( var j=0; j<GRID_SIZE_Y; j++)
				if(grid[i][j] == 0 )
					drawDarkImage(i, j);
				else
					drawLightImage(i, j);
//				grid[i][j](i, j);

		drawCharacterImage(character_x,character_y);
		drawGoalImage(goal_x, goal_y);
	}
	
	
	var updateStats = function()
	{
		character.moves++;
		$("#hero_moves").html(character.moves);
		
		var randomNumber = Math.floor(Math.random() * 100) + 1;

		if( character.moves % 10 == 0 && randomNumber < 15 )
		{
			character.strength++;
			$("#hero_strength").html(character.strength);
		}
		
		if( character.moves % 25 == 0 )
		{
			character.experience++;
			$("#hero_experience").html(character.experience);
		}
		
	}
	
	
	var makeImageData = function() {
		loadImage(imagesFilename, function() 
				{
			drawBoard();

		});
	};
	
	var loadImage = function( filename, callback )
	{
		imagesPalette.onload = function() 
			{ 
				callback(); 
			}; 
			imagesPalette.src = filename;
	};
	
	
	
    
    var doKeyDown = function(evt) {
        switch (evt.keyCode) {
            case 38:
                /* Up arrow was pressed */
            	if(character_y>0 && grid[character_x][character_y-1]==1)
            		{
            			character_y--;
            			updateStats();
            		}
                break;
            case 40:
                /* Down arrow was pressed */
            	if(character_y<GRID_SIZE_Y-1 && grid[character_x][character_y+1]==1)
            		{
            			character_y++;
            			updateStats();
            		}
                break;
            case 37:
                /* Left arrow was pressed */
            	if(character_x>0 && grid[character_x-1][character_y]==1)
            		{
            			character_x--;
            			updateStats();
            		}
                break;
            case 39:
                /* Right arrow was pressed */
            	if(character_x<GRID_SIZE_X-1 && grid[character_x+1][character_y]==1)
            		{
            			character_x++;
            			updateStats();
            		}
                break;
        }
        
        //$("#movecount").html("movecount: "+ character.moves);
        
        if( character_x == goal_x && character_y == goal_y )
        {
        	$("#message").html("You Won!!!!!!!");
        	$("#message").css("color", "red");
        }
    };
    window.addEventListener('keydown', doKeyDown, true);
    
    
    
	/*var grid = [[li, di, di, di, di, di, di, di, di, di, li, li],
	            [li, li, di, li, di, li, li, li, di, di, li, di],
	            [di, li, di, li, di, li, di, li, di, di, li, di],
	            [di, li, li, li, di, li, di, li, li, di, li, di],
	            [di, li, di, di, di, li, di, di, di, di, li, di],
	            [di, li, di, li, li, li, li, li, li, di, li, di],
	            [di, li, di, li, di, li, di, di, li, li, li, di],
	            [di, li, li, di, di, li, di, di, di, di, di, di],
	            [di, di, li, di, di, li, di, li, li, li, li, di],
	            [di, li, li, li, li, li, di, li, di, di, li, di],
	            [di, li, di, di, di, li, li, li, li, li, li, di],
	            [di, di, di, di, di, di, di, di, di, di, di, di]
	            
	            
	            ];*/
    

	
	var grid = 
				[
				[1, 1, 1, 1, 1, 1, 0 , 1, 1, 1, 0, 1],
	            [0, 0, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [1, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [0, 1, 0, 1, 0, 1, 0 , 1, 0, 1, 0, 1],
	            [0, 1, 1, 1, 1, 1, 1 , 1, 0, 1, 1, 1]
	            ];
	return Game;
})();

$(document).ready(function() {"use strict";
	new Game();
    InitStats();
});


function showCharAttributes() 
{
   'use strict';
   $("#hero_login").html(character.login);
   $("#hero_type").html(character.type);
   $("#hero_moves").html(character.moves);
   $("#hero_experience").html(character.experience);    
   $("#hero_health").html(character.health);   
   $("#hero_strength").html(character.strength);   
}

function showOrcAttributes()
{
	 $("#orc_login").html(orc.login);
	 $("#orc_type").html(orc.type);
	 $("#orc_moves").html(orc.moves);
	 $("#orc_experience").html(orc.experience);    
	 $("#orc_health").html(orc.health);   
	 $("#orc_strength").html(orc.strength);  
}

function readJson() {
	$.getJSON('/read', function( charAttributes ) {
		
		//heroChar = charAttributes(data.login);
		showCharAttributes(data.login, data.type, data.moves, data.experience, data.health, data.strength); 
	})
	.success(function() { showDebug('success'); })
	.error(function(jqXHR, textStatus, errorThrown) { 
		alert("error calling JSON. Try JSONLint or JSLint: " + textStatus); 
	})
	.complete(function() { console.log("csc: completed call to get index.json"); });
}

var writeJson = function() {
	/*showDebug($('#login').val());
	var userInput = { 
			
		login: $('#login').html(), 
		type: $('#type').html(), 
		moves: $('#moves').html(), 
		experience: $('#experience').html(),
		health: $('#health').html(),
		strength: $('#strength').html()
	};*/
	
	$.ajax({
		type: 'GET',
		url: '/write',
		dataType: 'json',
		data: character, 
		success: function(data) {
			showDebug(data.result);
		},
		error: showError      
	});
	
	$.ajax({
		type: 'GET',
		url: '/write',
		dataType: 'json',
		data: orc, 
		success: function(data) {
			showDebug(data.result);
		},
		error: showError      
	});
};

var showError = function(request, ajaxOptions, thrownError) {
	showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
	showDebug(request.status);
	showDebug(request.statusText);
	showDebug(request.getAllResponseHeaders());
	showDebug(request.responseText);
};

var showDebug = function(textToDisplay)
{
	$("#debug").append('<li>' + textToDisplay + '</li>');
};

var ShowMessage = function(textToDisplay)
{
	$("#message").append('<li>' + textToDisplay + '</li>');
};

var InitStats = function()
{
	character = 
	{ 
		login: $('#hero_login').html(), 
		type: $('#hero_type').html(), 
		moves: parseInt($('#hero_moves').html()), 
		experience: parseInt($('#hero_experience').html()),
		health: parseInt($('#hero_health').html()),
		strength: parseInt($('#hero_strength').html())
	};
	
	orc = 
	{
		login: $('#orc_login').html(), 
		type: $('#orc_type').html(), 
		moves: parseInt($('#orc_moves').html()), 
		experience: parseInt($('#orc_experience').html()),
		health: parseInt($('#orc_health').html()),
		strength: parseInt($('#orc_strength').html())
	};
};



var CalcStrike = function()
{
	$("#message").html('');
	
	var randomNumber = Math.floor(Math.random() * 100) + 1;
	var bonus = character.experience + character.strength;
	
	ShowMessage("randomNumber = " + randomNumber);
	ShowMessage("bonus = " + bonus);
	if( randomNumber + bonus > 50 )
	{
		ShowMessage("Hit!")
		orc.health = orc.health - 1;
		
		// hero has 25% chance to gain 1 experience point.
		var randomNumber2 = Math.floor(Math.random() * 100) + 1;
		ShowMessage("randomNumber2 = " + randomNumber2);
		if( randomNumber2 < 25 )
		{
			character.experience += 1;
			ShowMessage("Experience Gained!");
		}
	}
	
	showCharAttributes();
	showOrcAttributes();
};


