
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Prog 282 - bennett',
  						name: 'Chris Bennett' });
};

exports.page02 = function(req, res){
  res.render('Page02', { title: 'Page02',
  						name: 'Mr Wizard' });
};