
/*
$(document).ready(function() 
{
	"use strict";
	
	
	ELF.run.stats = new ELF.own.Stats();
	
	ELF.run.testobject = new ELF.own.TestObject();
	ELF.run.grid = new ELF.own.Grid();

	//ELF.run.stats.InitStats();
});
*/


var RunTestCases = function()
{
	
	// Unit Test Cases
	module("KeyboardInput Unit Test");
	test("KeyboardInput - Hero attributes loaded", function()
	{
		var actual = ELF.run.hero;
		var expected = null;
		notEqual(actual, expected, 'Is the Hero attributes loaded' + ELF.run.hero);
		
	});

	
	// Unit Test Cases
	module("Stats Unit Test");
	test("Stats - Hero attributes loaded", function()
	{
		var actual = ELF.run.hero;
		var expected = null;
		notEqual(actual, expected, 'Is the Hero attributes loaded' + ELF.run.hero);
		
	});
	
	
	module("Grid Unit Test");
	
	test("Grid - Grid Size", function()
			{
				var grid = ELF.run.grid.getGrid();
				var actual = grid.length;
				var expected = 20;
				equal(actual, expected, 'Is the size of the Grid 20');
				
			});
	
	test("Grid - Hero object exists", function()
			{
				var actual = ELF.run.hero;
				var expected = null;
				notEqual(actual, expected, 'Is the Hero attributes loaded' + ELF.run.hero);
			});
	
	test("Grid - Hero X > 0", function()
			{

				ok(ELF.run.hero.x >= 0, 'Is the Hero attributes loaded. x = ' + ELF.run.hero.x);
			});
	
	

}
