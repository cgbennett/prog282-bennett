
ELF.own.Grid = (function() 
{

	
	// Private variables (in closure)
	var context = null;
	var rectSize = 25;
	var width = -1;
	var height = -1;
	var count = 0;
	var score = 0;
	var redrawGrassCounter = 0;
	var moveNPCCounter = 0;
	
	// When frame counter reaches 50 then redraw grass.  Hope this gives the visual of grass blowing in wind.
	var REDRAW_GRASS = 5; 
	var MOVE_NPCS = 15;
	var currentgrid = 1;
	
	// Flag to control if NPCs can move.  They cannot move during combat.
	var flagNPCsMove = true;

	
    var npc = [			
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
              ];
              
          	
      var grid = 
          	[
          	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
          	[1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
          	[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
          	[0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0],
          	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
          	];
      
      var grid2 =
      [[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2],
       [2,1,2,2,1,1,0,2,2,2,2,1,1,0,1,1,2,0,2,1],
       [0,2,2,1,2,1,0,2,1,1,0,1,2,0,1,0,2,0,2,0],
       [0,1,1,1,2,2,0,1,2,2,0,2,2,0,1,0,2,0,2,0],
       [0,2,1,2,2,2,0,2,2,2,0,2,2,0,2,0,1,0,2,0],
       [0,1,2,2,2,2,0,2,1,1,0,1,2,0,1,0,1,0,1,0],
       [0,2,2,2,2,2,0,1,2,1,0,1,2,0,2,0,1,0,2,0],
       [0,2,1,2,1,2,0,1,2,2,0,2,2,0,1,0,1,0,2,0],
       [0,1,1,2,1,2,0,2,1,1,0,2,1,0,2,0,2,0,2,0],
       [0,0,0,1,0,1,0,2,0,1,0,2,1,0,2,0,1,0,1,0],
       [0,1,0,2,0,2,0,1,0,2,0,2,2,0,1,0,1,1,1,0],
       [0,2,0,2,0,2,0,1,0,1,0,2,2,0,1,0,2,0,2,0],
       [0,2,0,2,0,2,0,1,0,1,0,2,2,0,2,0,1,0,1,0],
       [0,1,0,2,0,1,0,1,0,1,0,2,2,0,2,2,2,0,2,0],
       [0,2,0,2,0,2,0,1,0,2,0,1,2,2,1,0,1,0,1,0],
       [0,1,0,2,0,1,0,1,0,1,0,1,1,0,2,0,2,0,1,0],
       [0,2,0,2,0,1,0,1,0,1,0,1,1,0,1,0,2,0,2,0],
       [0,2,0,1,0,1,0,2,0,1,0,2,2,0,2,0,1,0,2,0],
       [0,2,0,1,0,2,2,1,0,1,0,2,2,2,1,0,1,2,1,0],
       [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]];
      
      
      var grid3 = [[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2],
                   [1,1,1,1,2,2,0,2,1,2,2,2,1,0,1,2,1,0,2,2],
                   [0,2,1,2,2,2,0,1,1,2,0,2,1,0,2,0,2,0,2,0],
                   [0,2,1,2,1,1,0,2,1,1,0,2,2,0,1,0,1,0,2,0],
                   [0,2,2,1,2,1,0,2,2,2,0,1,2,0,1,0,2,0,2,0],
                   [0,1,1,2,2,2,0,2,2,1,0,1,2,0,1,0,2,0,1,0],
                   [0,2,1,2,2,1,0,2,2,1,0,1,1,0,1,0,2,0,1,0],
                   [0,1,2,2,2,1,0,2,2,2,0,1,2,0,2,0,2,0,1,0],
                   [0,2,1,2,1,2,0,2,2,1,0,1,2,0,2,0,2,0,1,0],
                   [0,0,0,2,0,2,0,1,0,2,0,1,2,0,2,0,1,0,2,0],
                   [0,2,0,1,0,1,0,2,0,1,0,2,2,0,1,0,1,2,2,0],
                   [0,2,0,2,0,1,0,2,0,1,0,2,1,0,1,0,2,0,2,0],
                   [0,1,0,2,0,2,0,2,0,1,0,2,2,0,2,0,2,0,1,0],
                   [0,2,0,1,0,2,0,1,0,2,0,2,1,0,1,2,1,0,2,0],
                   [0,1,0,2,0,2,0,2,0,2,0,2,2,2,2,0,1,0,2,0],
                   [0,1,0,1,0,1,0,2,0,1,0,1,1,0,1,0,1,0,1,0],
                   [0,1,0,2,0,2,0,1,0,1,0,1,2,0,2,0,2,0,1,0],
                   [0,1,0,2,0,2,0,2,0,1,0,2,1,0,2,0,1,0,2,0],
                   [0,2,0,2,0,2,1,1,0,2,0,2,1,1,1,0,2,2,1,0],
                   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]];
      
      var grid4 = [[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2],
                   [1,2,1,1,1,2,0,1,1,2,2,2,2,0,1,2,1,0,2,2],
                   [0,2,2,1,1,2,0,2,2,2,0,2,1,0,2,0,2,0,1,0],
                   [0,1,1,1,2,2,0,2,1,2,0,2,1,0,2,0,2,0,1,0],
                   [0,2,1,1,2,1,0,2,1,1,0,2,1,0,2,0,1,0,2,0],
                   [0,2,2,2,2,2,0,2,1,2,0,1,1,0,1,0,1,0,2,0],
                   [0,2,2,2,2,1,0,1,1,1,0,1,2,0,1,0,1,0,1,0],
                   [0,2,2,2,2,2,0,2,2,2,0,2,2,0,1,0,1,0,1,0],
                   [0,1,2,1,2,1,0,2,1,1,0,2,1,0,2,0,2,0,1,0],
                   [0,0,0,2,0,2,0,2,0,2,0,2,2,0,1,0,1,0,1,0],
                   [0,2,0,2,0,2,0,2,0,1,0,1,2,0,1,0,1,1,1,0],
                   [0,2,0,1,0,2,0,2,0,2,0,2,1,0,2,0,2,0,1,0],
                   [0,2,0,1,0,1,0,2,0,2,0,2,1,0,2,0,1,0,2,0],
                   [0,2,0,1,0,2,0,1,0,2,0,2,1,0,2,1,2,0,2,0],
                   [0,1,0,1,0,2,0,1,0,2,0,2,2,2,2,0,1,0,1,0],
                   [0,1,0,2,0,2,0,2,0,2,0,2,1,0,2,0,1,0,1,0],
                   [0,1,0,2,0,2,0,1,0,1,0,1,2,0,1,0,2,0,2,0],
                   [0,1,0,1,0,1,0,2,0,2,0,1,1,0,1,0,1,0,1,0],
                   [0,1,0,1,0,1,1,1,0,1,0,2,2,1,2,0,2,1,1,0],
                   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]];
      
	
	goal_x = 0;
	goal_y = 19;

	

	var imagesPalette = new Image();
	var imagesFilename = "public/images.gif";
	
	var npcImagesPalette = new Image();
	var npcImagesFilename = "public/npc.gif";
	
	var terrainImagesPalette = new Image();
	var terrainImagesFilename = "public/terrainAtlas.gif";


	// Constructor has Caps, must be called with new, returned from App
	function Grid() 
	{
		$('#divCombatCanvas').hide('Slide');
		
		MakeRandomGrids();
		
		ELF.run.keyboardinput = new ELF.own.KeyboardInput(this);
		window.addEventListener('keydown', ELF.run.keyboardinput.doKeyDown, true);
		
		context = getCanvas();
		makeImageData();
		
		
		
		
	}
	
	Grid.prototype.SetNPCsMoveFlag = function( flag )
	{
		flagNPCsMove = flag;
	}
	
	Grid.prototype.DetectCollision = function(x, y)
	{
		if( npc[x][y]>0 )
			DoCombat(npc[x][y]);
	}
	
	var DoCombat = function( npc_id )
	{
		flagNPCsMove = false;
		
		ELF.run.stats.readOrcJsonPublic(npc_id);
		
		
		$('#divMainCanvas').hide('Slide');
		$('#divCombatCanvas').show('Slide');
		var combat_canvas = document.getElementById('combatCanvas');
		var combat_context = combat_canvas.getContext("2d");
		combat_context.fillStyle="#000000";
		combat_context.fillRect(0,0,600, 500);
		
		combat_context.drawImage(imagesPalette, rectSize, 0, 
				rectSize, rectSize,
				0, 0, 
				300, 500);
		
		//combat_context.drawImage(imagesPalette, rectSize, 0, 
		//		rectSize, rectSize,
		//		0, 0, 
		//		300, 500);
		//alert(npc_id);
		if( npc_id == 1 )
			combat_context.drawImage(npcImagesPalette, 0, rectSize, 
					rectSize, rectSize, 
					300, 0, 
					300, 500);
		else if( npc_id == 2 )
		{
			combat_context.drawImage(npcImagesPalette, 0, 2*rectSize, 
					rectSize, rectSize, 
					300, 0, 
					300, 500);
		}
		else if( npc_id == 3 )
		{
			combat_context.drawImage(npcImagesPalette, rectSize, 2*rectSize, 
					rectSize, rectSize,
					300, 0, 
					300, 500);
		}
		else if( npc_id == 4 )
		{
			combat_context.drawImage(npcImagesPalette, rectSize, rectSize, 
					rectSize, rectSize,
					300, 0, 
					300, 500);
		}
		
		//alert('COMBAT!');
		//combat_context.drawImage
	}
	
	Grid.prototype.getGrid = function()
	{
		return grid;
	}

	Grid.prototype.getNPC = function()
	{
		return npc;
	}
	
	
	Grid.prototype.SaveGrid = function()
	{
		ELF.run.stats.writeJsonGrid();
	}
	
	var getCanvas = function() 
	{
		canvas = document.getElementById('mainCanvas');
		if (canvas !== null) {
			width = canvas.width;
			height = canvas.height;
			context = canvas.getContext('2d');
			setInterval(draw, 100
					);
			return context;
		} else {
			$("#debugs").css({
				backgroundColor : "blue",
				color: "yellow"
			});
			$("#debugs").html("Could not retrieve Canvas");
			return null;
		}
	}
	
	
	// This function creates copies of the 2-D grid matrix, but randomizes the grass
	// icons, so it give the animation of grass blowing in the wind.  
	// I cannot seem to copy the grids appropriately... and I don't know why.  Hmmmmm....
	var MakeRandomGrids = function()
	{
	/*	grid2 = $.extend( true, {}, grid );
		grid3 = $.extend( true, {}, grid );
		grid4 = $.extend( true, {}, grid );
		for (var i=0; i<grid.length; i++)
			for( var j=0; j<grid[i].length; j++)
				{
					if( grid[i][j] > 0 )
					{
						var randomNumber = Math.floor(Math.random() * 2) + 1;
						grid2[i][j] = randomNumber;
					}
				}
		for (var i=0; i<grid.length; i++)
			for( var j=0; j<grid[i].length; j++)
				{
					if( grid[i][j] > 0 )
					{
						var randomNumber = Math.floor(Math.random() * 2) + 1;
						grid3[i][j] = randomNumber;
					}
				}
		for (var i=0; i<grid.length; i++)
			for( var j=0; j<grid[i].length; j++)
				{
					if( grid[i][j] > 0 )
					{
						var randomNumber = Math.floor(Math.random() * 2) + 1;
						grid4[i][j] = randomNumber;
					}
				}*/
	}
	
	var draw = function() 
	{
		if( redrawGrassCounter > REDRAW_GRASS )
		{
			// Loop through grid and randomly change grass.
			if( currentgrid == 1 || currentgrid == 4 )
				{
				grid = grid2;
				currentgrid = 2;
				}
			else if( currentgrid == 2 )
				{
				grid = grid3;
				currentgrid = 3;
				}
			else if( currentgrid == 3 )
				{
				grid = grid4;
				currentgrid = 4;
				}

			// This code recreates the grid every second or so (depends on REDRAW_GRASS value)
			// and puts in random grass value.  It works, but now the keyboard arrows keys
			// don't work sometimes.   I think it's because this code is so CPU intensive
			// that it blocks keyboard strokes getting inputed in javascript event handler.
			// I'll try a different approach.
			/*for (var i=0; i<grid.length; i++)
				for( var j=0; j<grid[i].length; j++)
					{
						if( grid[i][j] > 0 )
						{
							var randomNumber = Math.floor(Math.random() * 2) + 1;
							grid[i][j] = randomNumber;
						}
					}*/
			
			redrawGrassCounter=0;
		}
		
		
		if( moveNPCCounter > MOVE_NPCS && flagNPCsMove )
		{
			MoveNPCs();
			moveNPCCounter = 0;
		}
		
		moveNPCCounter++;
		redrawGrassCounter++;
		drawBoard();
	}
	
	
	var MoveNPCs = function()
	{
		for (var i=0; i<npc.length; i++)
			for( var j=0; j<npc[i].length; j++)
			{
				if( npc[i][j] > 0 )
				{
					var rand = Math.floor(Math.random() * 4) + 1;
					if( rand == 1 && j>0 ) // Move Up
					{
						if( npc[i][j-1] == 0 )
						{
							npc[i][j-1] = npc[i][j]-100;
							npc[i][j] = 0;
						}
					}
					else if (rand == 2 && j<npc.length-2) // Move Down
					{
						if( npc[i][j+1] == 0 )
							{
							npc[i][j+1] = npc[i][j]-100;
							npc[i][j] = 0;
							}
					}
					else if( rand == 3 && i>0 ) // Move Left
					{
						if( npc[i-1][j] == 0 )
						{
							npc[i-1][j] = npc[i][j]-100;
							npc[i][j] = 0;
						}
					}
					else if( rand == 4 && i<npc.length-2) // Move Right
					{
						if( npc[i+1][j] == 0 )
						{
							npc[i+1][j] = npc[i][j]-100;
							npc[i][j] = 0;
						}
					}
				}
			}

	    for( var i=0; i<npc.length; i++ )
		for( var j=0; j<npc[i].length; j++ )
		    if( npc[i][j] < 0 )
			npc[i][j] += 100;

            ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
	}
	
	var drawDarkImage =  function(x,y)
	{
		context.drawImage(imagesPalette, 0, rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
	
	}
	
	var drawGrassImage1 = function(x,y)
	{
		context.drawImage(imagesPalette, 0, 0, rectSize, rectSize, x*rectSize,y*rectSize, rectSize, rectSize);
	}
	
	var drawGrassImage2 = function(x,y)
	{
		//context.drawImage(imagesPalette, 0, rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
		context.drawImage(imagesPalette, 2*rectSize, 2*rectSize, rectSize, rectSize, x*rectSize,y*rectSize, rectSize, rectSize);
	}
	
	var drawCharacterImage = function(x,y)
	{
		context.drawImage(imagesPalette, rectSize, 0, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawGoalImage = function(x,y)
	{
		context.drawImage(imagesPalette, rectSize, rectSize, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawNPC1 = function(x,y)
	{
		context.drawImage(npcImagesPalette, 0, rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawNPC2 = function(x,y)
	{
		context.drawImage(npcImagesPalette, 0, 2*rectSize, rectSize, rectSize, x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawNPC3 = function(x,y)
	{
		context.drawImage(npcImagesPalette, rectSize, 2*rectSize, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	
	var drawTree = function(x,y)
	{
		//context.drawImage(terrainImagesPalette, rectSize, 1024-rectSize, 1024-rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
		context.drawImage(npcImagesPalette, rectSize, rectSize, rectSize, rectSize,x*rectSize, y*rectSize, rectSize, rectSize);
	}
	

	Grid.prototype.hasReachedGoal = function()
	{
        
        if( ELF.run.hero.x == goal_x && ELF.run.hero.y == goal_y )
        {
          	$("#message").html("You Won!!!!!!!");
          	$("#message").css("color", "red");
          	alert("You Won!!!!!");
        }
		
	}
	

	var drawBoard = function()
	{		
		
		// Draw GRID
		for (var i=0; i<grid.length; i++)
			for( var j=0; j<grid[i].length; j++)
				if(grid[i][j] == 0 )
					drawDarkImage(i, j);
				else if(grid[i][j] == 1 )
					drawGrassImage1(i, j);
				else if(grid[i][j] == 2 )
					drawGrassImage2(i, j);
//				grid[i][j](i, j);

		// Draw NPCs
		for (var i=0; i<npc.length; i++)
			for( var j=0; j<npc[i].length; j++)
				if(npc[i][j] == 1 )
					drawNPC1(i, j);
				else if(npc[i][j] == 2 )
					drawNPC2(i, j);
				else if(npc[i][j] == 3 )
					drawNPC3(i, j);
				else if(npc[i][j] == 4 )
					drawTree(i, j);
		
		drawCharacterImage(ELF.run.hero.x,ELF.run.hero.y);
		drawGoalImage(goal_x, goal_y);
		//drawTree(10,11);
	}
	
	
	var updateStats = function()
	{
		character.moves++;
		$("#hero_moves").html(character.moves);
		
		var randomNumber = Math.floor(Math.random() * 100) + 1;

		if( character.moves % 10 == 0 && randomNumber < 15 )
		{
			character.strength++;
			$("#hero_strength").html(character.strength);
		}
		
		if( character.moves % 25 == 0 )
		{
			character.experience++;
			$("#hero_experience").html(character.experience);
		}
		
	}
	
	
	var makeImageData = function() 
	{
		loadImage(imagesFilename, function() 
				{
			drawBoard();
		});
		
		npcImagesPalette.src = npcImagesFilename;
		terrainImagesPalette.src = terrainImagesFilename;

	}
	
	var loadImage = function( filename, callback )
	{
		imagesPalette.onload = function() 
			{ 
				callback(); 
			}; 
			imagesPalette.src = filename;

	}
	


	Grid.prototype.ShowCanvas = function()
	{
		$('#divMainCanvas').show('Slide');
	}
	
	Grid.prototype.HideCanvas = function()
	{
		$('#divMainCanvas').hide('Slide');
	}
	
  
	return Grid;
})();


var ShowGrid = function()
{
	if( $('#divMainCanvas').is(":visible") )
		$('#divMainCanvas').hide('Slide');
	else 
		$('#divMainCanvas').show('Slide');
}

var ShowCombat = function()
{
	if( $('#divCombatCanvas').is(":visible") )
		$('#divCombatCanvas').hide('Slide');
	else
		$('#divCombatCanvas').show('Slide');
}

var ShowCharAttributes = function()
{
	if( $('#divAttributes').is(":visible") )
		$('#divAttributes').hide('Slide');
	else
		$('#divAttributes').show('Slide');
}

