 
ELF.own.KeyboardInput = (function() 
{
	var grid = null;
	
	function KeyboardInput(maingrid)
	{
		grid = maingrid.getGrid();

	}

	KeyboardInput.prototype.doKeyDown = function(evt) 
	{
        switch (evt.keyCode) 
        {
            case 38:
                /* Up arrow was pressed */
            	if(ELF.run.hero.y>0 && grid[ELF.run.hero.x][ELF.run.hero.y-1]==1)
            		{
            			ELF.run.hero.y--;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            		}
                break;
            case 40:
                /* Down arrow was pressed */
            	if(ELF.run.hero.y<grid.length-1 && grid[ELF.run.hero.x][ELF.run.hero.y+1]==1)
            		{
            			ELF.run.hero.y++;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            		}
                break;
            case 37:
                /* Left arrow was pressed */
            	if(ELF.run.hero.x>0 && grid[ELF.run.hero.x-1][ELF.run.hero.y]==1)
            		{
            			ELF.run.hero.x--;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            		}
                break;
            case 39:
                /* Right arrow was pressed */
            	if(ELF.run.hero.x<grid[0].length-1 && grid[ELF.run.hero.x+1][ELF.run.hero.y]==1)
            		{
            			ELF.run.hero.x++;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            		}
                break;
        }
        
        ELF.run.grid.hasReachedGoal();
	}
	

	
	KeyboardInput.prototype.UnitTests = function()
	{

	}
	
	
	return KeyboardInput;
})();

