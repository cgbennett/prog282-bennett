var App = ( function() {
		// Private variable
		var MAX = 3;

		function App() {
			for (var i = 0; i < MAX; i++) {
				for (var j = 0; j < MAX; j++) {
					var span = "#span" + j + i;
					$(span).html(span);
				}

			}

			window.addEventListener('keydown', doKeyDown, true);

		}

		var doKeyDown = function(evt) {
			switch( evt.keyCode ) {
				case 38:
					// Up arrow
					$("#debug").html("Up arrow");
					break;
				case 40:
					// Down arrow
					$("#debug").html("Down arrow");
					break;
				case 37:
					// Left arrow.
					$("#debug").html("Left arrow");
					break;
				case 39:
					// Right arrow.
					$("#debug").html("Right arrow");
					break;

			}
		}

		return App;

	}());

$(document).ready(function() {
	App();
});
