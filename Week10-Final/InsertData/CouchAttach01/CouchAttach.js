var couch = require('./Library/CouchCode')
var fs = require('fs');

//var dbName = 'prog282bennett_test';

function run() {
	var docName = process.argv[2];
	var fileName = process.argv[3];	
	var couchdb_url = process.argv[4];
	var dbname = process.argv[5];
	
	
	couch.couchCode.SetCouchDB_URL(couchdb_url);

	fs.exists(fileName, function(exists) {
		if (exists) {
			var data = fs.readFileSync(fileName);
			
			couch.couchCode.createDatabase(dbname, function(error) {
				if (!error) {
					couch.couchCode.couchAttach(null, docName, data, dbname);
				} else {
				  couch.couchCode.reportError(error);			    
				}
			});
		} else {
			console.log('could not find: ' + fileName);
		}
	})
};

function explain() {
	console.log('\n\nPlease pass in the docName you want to use in');
	console.log('couchDb and the name of the document you want');
	console.log('send to couchDb\n');
	console.log('Example: ');
	console.log('  node CouchAttach.js index index.html');
};

if (process.argv.length === 6) 
{
	
	run();
} else {
	explain();
}