set DBNAME=prog282bennett
echo %DBNAME%

rem set COUCHDB_URL=http://54.243.217.141:5984/
set COUCHDB_URL=http://localhost:5984/
echo %COUCHDB_URL%


call node ./CouchAttach01/CouchAttach.js Grid.html ../public/Grid.html %COUCHDB_URL% %DBNAME%
call node ./CouchAttach01/CouchAttach.js OrcTable.html ../public/OrcTable.html  %COUCHDB_URL% %DBNAME%
call node ./CouchAttach01/CouchAttach.js HeroTable.html ../public/HeroTable.html %COUCHDB_URL% %DBNAME%
call node ./CouchAttach01/CouchAttach.js TestGrid.html ../public/TestGrid.html %COUCHDB_URL% %DBNAME%
call node ./CouchAttach01/CouchAttach.js TestScoring.html ../public/TestScoring.html %COUCHDB_URL% %DBNAME%

call node ./CouchAttach01/CouchAttach.js images.gif ../images/images.gif %COUCHDB_URL% %DBNAME%

call node ./CouchDoc01/CouchDoc.js npcgrid ./npcgrid.json %COUCHDB_URL% %DBNAME%
call node ./CouchDoc01/CouchDoc.js grid ./grid.json %COUCHDB_URL% %DBNAME%
call node ./CouchDoc01/CouchDoc.js hero ./hero.json %COUCHDB_URL% %DBNAME%
call node ./CouchDoc01/CouchDoc.js orc ./orc.json %COUCHDB_URL% %DBNAME%



