



ELF.own.Display = ( function() 
{
	function Display() 
	{
	
	}
	
	Display.prototype.ShowGrid = function()
	{
		if( $('#divMainCanvas').is(":visible") )
			$('#divMainCanvas').hide('Slide');
		else 
			$('#divMainCanvas').show('Slide');
	}
	
	Display.prototype.ShowCombat = function()
	{
		if( $('#divCombatCanvas').is(":visible") )
			$('#divCombatCanvas').hide('Slide');
		else
			$('#divCombatCanvas').show('Slide');
	}
	
	Display.prototype.ShowCharAttributes = function()
	{
		if( $('#divAttributes').is(":visible") )
			$('#divAttributes').hide('Slide');
		else
			$('#divAttributes').show('Slide');
	}

	
	return Display;
})();

ELF.run.display = new ELF.own.Display();