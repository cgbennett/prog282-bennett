
ELF.own.Stats = (function() 
{
	
	function Stats()
	{
		readHeroJson();
		readOrcJson();
	}

	function showHeroAttributes() 
	{
	   'use strict';
	   $("#hero_login").html(ELF.run.hero.login);
	   $("#hero_type").html(ELF.run.hero.type);
	   $("#hero_moves").html(ELF.run.hero.moves);
	   $("#hero_experience").html(ELF.run.hero.experience);    
	   $("#hero_health").html(ELF.run.hero.health);   
	   $("#hero_strength").html(ELF.run.hero.strength);   
	}

	Stats.prototype.showOrcAttributes = function()
	{
		showNpcAttributes();
	}
	
	function showNpcAttributes() 
	{
		 'use strict';
		 $("#orc_login").html(ELF.run.orc.login);
		 $("#orc_type").html(ELF.run.orc.type);
		 $("#orc_moves").html(ELF.run.orc.moves);
		 $("#orc_experience").html(ELF.run.orc.experience);    
		 $("#orc_health").html(ELF.run.orc.health);   
		 $("#orc_strength").html(ELF.run.orc.strength);  
	}
	
	// Convert the hero and orc attributes from strings to integers,
	// so we can use them for calculations.
	function convertToInts(hero)
	{
		hero.x = parseInt(hero.x);
		hero.y = parseInt(hero.y);
		hero.moves = parseInt(hero.moves);
		hero.experience = parseInt(hero.experience);
		hero.health = parseInt(hero.health);
		hero.strength = parseInt(hero.strength);
		hero.x_offset = parseInt(hero.x_offset);
		hero.y_offset = parseInt(hero.y_offset);
		
		return hero;
	}
	
	Stats.prototype.readHeroJsonPublic = function() 
	{
		readHeroJson();
	}
	
	Stats.prototype.readOrcJsonPublic = function(npc_id) 
	{
		readOrcJson(npc_id);
	}

	var readHeroJson = function()
	{
		$.getJSON('/readHero', function( charAttributes ) 
		{
			ELF.run.hero = convertToInts(charAttributes);
			showHeroAttributes(); 
		})
		.success(function() { showDebug('success'); })
		.error(function(jqXHR, textStatus, errorThrown) 
		{ 
			alert("error calling JSON. Try JSONLint or JSLint: " + textStatus + errorThrown); 
		})
		.complete(function() { console.log("csc: completed call to get index.json"); });
	
	}
	
	var readOrcJson = function(npc_id) 
	{
		$.getJSON('/readOrc', function( charAttributes ) 
		{
			ELF.run.orc = convertToInts(charAttributes);
			showNpcAttributes(); 
		})
		.success(function() 
		{ 
			showDebug('success'); 
			ELF.run.stats.showOrcAttributes();
		})
		.error(function(jqXHR, textStatus, errorThrown) 
		{ 
			alert("error calling JSON. Try JSONLint or JSLint: " + textStatus); 
		})
		.complete(function() { console.log("csc: completed call to get index.json"); });
	}

	Stats.prototype.writeJsonHero = function() 
	{
		$.ajax({
			type: 'GET',
			url: '/writeHero',
			dataType: 'json',
			data: ELF.run.hero, 
			success: function(data) {
				showDebug(data.result);
			},
			error: showError      
		});

	}
	
	Stats.prototype.writeJsonOrc = function() 
	{
		$.ajax({
			type: 'GET',
			url: '/writeOrc',
			dataType: 'json',
			data: ELF.run.orc, 
			success: function(data) {
				showDebug(data.result);
			},
			error: showError      
		});
	}
	
	
	Stats.prototype.writeJsonGrid = function() 
	{
		var grid_matrix = ELF.run.grid.getGrid();
		var gridstring = JSON.stringify(grid_matrix)
		var data = { grid: gridstring }
		$.ajax({
			type: 'POST',
			url: '/writeGrid',
			dataType: 'json',
			data: data, 
			success: function(data) {
				showDebug(data.result);
			},
			error: showError      
		});
	}
	
	Stats.prototype.writeJsonNpcGrid = function() 
	{
		var grid_matrix = ELF.run.grid.getNPC();
		var gridstring = JSON.stringify(grid_matrix)
		var data = { grid: gridstring }
		$.ajax({
			type: 'POST',
			url: '/writeNpcGrid',
			dataType: 'json',
			data: data, 
			success: function(data) {
				showDebug(data.result);
			},
			error: showError      
		});
	}
	

	var showError = function(request, ajaxOptions, thrownError) 
	{
		showDebug("Error occurred: = " + ajaxOptions + " " + thrownError );
		showDebug(request.status);
		showDebug(request.statusText);
		showDebug(request.getAllResponseHeaders());
		showDebug(request.responseText);
	}
	
	var showDebug = function(textToDisplay)
	{
		$("#debug").append('<li>' + textToDisplay + '</li>');
	}

	var ShowMessage = function(textToDisplay)
	{
		$("#message").append('<li>' + textToDisplay + '</li>');
	}

	Stats.prototype.InitStats = function()
	{
		ELF.run.hero = 
		{ 
			login: $('#hero_login').html(), 
			type: $('#hero_type').html(), 
			moves: parseInt($('#hero_moves').html()), 
			experience: parseInt($('#hero_experience').html()),
			health: parseInt($('#hero_health').html()),
			strength: parseInt($('#hero_strength').html())
		};
		
		ELF.run.orc = 
		{
			login: $('#orc_login').html(), 
			type: $('#orc_type').html(), 
			moves: parseInt($('#orc_moves').html()), 
			experience: parseInt($('#orc_experience').html()),
			health: parseInt($('#orc_health').html()),
			strength: parseInt($('#orc_strength').html())
		};
	}


	Stats.prototype.UpdateStats = function()
	{
		ELF.run.hero.moves++;
		$("#hero_moves").html(ELF.run.hero.moves);
		
		var randomNumber = Math.floor(Math.random() * 100) + 1;

		if( ELF.run.hero.moves % 10 == 0 && randomNumber < 15 )
		{
			ELF.run.hero.strength++;
			$("#hero_strength").html(ELF.run.hero.strength);
		}
		
		if( ELF.run.hero.moves % 25 == 0 )
		{
			ELF.run.hero.experience++;
			$("#hero_experience").html(ELF.run.hero.experience);
		}
		
	}

	Stats.prototype.CalcStrike = function()
	{
		$("#message").html('');
		
		if( ELF.run.playSoundFlag == true )
		{
	    	var strikeSound = new Audio('sound/strike.wav');
	    	strikeSound.play();
		}
		
		var randomNumber = Math.floor(Math.random() * 100) + 1;
		var bonus = ELF.run.hero.experience + ELF.run.hero.strength;
		
		ShowMessage("randomNumber = " + randomNumber);
		ShowMessage("bonus = " + bonus);
		if( randomNumber + bonus > 50 )
		{
			ShowMessage("Hit!")
			ELF.run.orc.health = ELF.run.orc.health - 1;
			
			// hero has 25% chance to gain 1 experience point.
			var randomNumber2 = Math.floor(Math.random() * 100) + 1;
			ShowMessage("randomNumber2 = " + randomNumber2);
			if( randomNumber2 < 25 )
			{
				ELF.run.hero.experience += 1;
				ShowMessage("Experience Gained!");
			}
		}
		
		showHeroAttributes();
		showNpcAttributes();
		
		// if grid object isn't created then don't go further.
		if( ELF.run.grid == null || ELF.run.grid == undefined )
			return;
		
		if(ELF.run.orc.health <= 0 )
		{
			$('#divMainCanvas').show('Slide');
			$('#divCombatCanvas').hide('Slide');
			
			var npc = ELF.run.grid.getNPC();
			npc[ELF.run.hero.x][ELF.run.hero.y] = 0;
			
			if( ELF.run.playSoundFlag == true )
			{
				// Play Victory sound
		    	var victorySound = new Audio('sound/victory2.wav');
		    	victorySound.play();
			}
			
			//alert('Victory!');
			
			// NPCs can move again.
			ELF.run.grid.SetNPCsMoveFlag(true);
		}
		
		
	}
	
	Stats.prototype.UnitTests = function()
	{
	}
	
	return Stats;
})();

