var express = require('express'), 
	routes = require('./routes'),
	user = require('./routes/user'),
	http = require('http'),
	path = require('path');

var app = express();
var fs = require('fs'); 
var openid = require('openid');
var querystring = require('querystring');
var handlebars = require('handlebars');





//var nano = require('nano')('http://54.243.217.141:5984');
var nano = require('nano')('http://localhost:5984');

var dbName = 'prog282bennett'; 

 
var port = process.env.PORT || 30025; 

var url = require('url');
var querystring = require('querystring');
var relyingParty = new openid.RelyingParty('http://localhost:30025/go', // Verification
//var relyingParty = new openid.RelyingParty('http://54.243.217.141:30025/go', // Verification
// 54.243.217.141 
		// URL
		// (yours)
		null, // Realm (optional, specifies realm for OpenID authentication)
		false, // Use stateless verification
		false, // Strict mode
		[]);

var readHtmlfromCouchDB = function( docname, callback )
{
	
	var prog = nano.db.use(dbName);
    prog.get(docname, 
    		{ revs_info : true }, 
    		callback
    		);
}


app.use(express.cookieParser('1234567ASDFG'));
app.use(express.session({ cookie: { maxAge: 60 * 60 * 1000 }}));

// Load all html files and put in global variables.
//var herotable_html = String(fs.readFileSync('public/HeroTable.html'));
//var orctable_html = String(fs.readFileSync('public/OrcTable.html'));
//var grid_html = String(fs.readFileSync('public/Grid.html'));

var gridgame_html = String(fs.readFileSync('public/GridGame.html'));
var usertestcases_html = String(fs.readFileSync('public/UnitTestCases.html'));


var testscoring_html = null;  
var testgrid_html = null; 
var herotable_html = null;
var orctable_html = null;
var grid_html = null;


var getAttachedDocument = function(docName, dbName, setresultfunc) 
{
	   console.log('getAttachedHtml called');   
	   var prog = nano.db.use(dbName);
	   prog.attachment.get(docName, docName, function(err, body) 
			{   
	        	if (!err) 
	        	{
	        		//console.log("---------------------------------");
	        		//console.log(body);
	        		//console.log("---------------------------------");
	        		var html = body;
	        		setresultfunc(html);
	        	} 
	        	else 
	        	{
	        		console.log('Error');
	        		console.log(err);
	            }
	    }); 
 };
 
getAttachedDocument( 'Grid.html', dbName, function( result )
		{
	  		grid_html = result;
		});
getAttachedDocument( 'OrcTable.html', dbName, function( result )
	 	{
			orctable_html = result;
	 	});
getAttachedDocument( 'HeroTable.html', dbName, function( result )
	 	{
			herotable_html = result;
	 	});
getAttachedDocument( 'TestGrid.html', dbName, function( result )
	 	{
			testgrid_html = result;
	 	});
getAttachedDocument( 'TestScoring.html', dbName, function( result )
	 	{
			testscoring_html = result;
	 	});


// We need this here to make POST call work
app.use(express.bodyParser());

app.get('/', function(req, res) 
{
	// Deliver an OpenID form on all other URLs
	res.writeHead(200);
	res
			.end('<!DOCTYPE html><html><body>'
					+ '<form method="get" action="/authenticate">'
					+ '<p>Login using OpenID</p>'
					+ '<p>OpenID: <input name="openid_identifier" /></p>'
					+ '<p>First Name: <input name="firstname" /></p>'
					+ '<input type="submit" value="Login" />'
					+ '</form></body></html>');
});

app.get('/go', function(request, response) 
{
	'use strict';
	console.log('Go called');
	try 
	{
		/*var herotable = String(fs.readFileSync('public/HeroTable.html'));
		var orctable = String(fs.readFileSync('public/OrcTable.html'));
		var grid = String(fs.readFileSync('public/Grid.html'));
		var html = String(fs.readFileSync('public/GridGame.html'));
		*/
		
		// Set Session object
		request.session.open_id = request.query['openid.identity'];

		if(request.session.firstname == null || request.session.firstname == undefined )
			request.session.firstname = 'Anonymous';
		
		var context = 
			{
				FirstName: request.session.firstname,
				HeroTable: herotable_html,
				OrcTable: orctable_html,
				Grid: grid_html
			}
		
		var result = replaceHtml( context, gridgame_html );
		
		response.writeHeader(200, 
			{
			"Content-Type" : "text/html"
			});
		response.end(result);
	} 
	catch (error) 
	{
		console.log(error);
	}
});


app.get('/launchSecondHtmlFile', function(request, response) 
{
	console.log('launchSecondHtmlFile called');
	try {
		var html = fs.readFileSync('Public/SecondFile.html');
		response.writeHeader(200, 
				{
				"Content-Type" : "text/html"
				});
			response.end(html);
	} 
	catch (error) 
	{
		console.log(error);
	}

});


app.get('/UnitTestCases', function(request, response) 
{
	'use strict';
	
	console.log('UnitTestCases called');
	try 
	{
		//var grid = String(fs.readFileSync('public/Grid.html'));
		//var html = String(fs.readFileSync('public/UnitTestCases.html'));
		
		var context = 
			{
				Grid: grid_html
			}
		
		var result = replaceHtml( context, usertestcases_html );
		
		response.writeHeader(200, 
			{
				"Content-Type" : "text/html"
			});
		response.end(result);
	} 
	catch (error) 
	{
		console.log(error);
	}

});


app.get('/GridGame', function(request, response) 
{
	'use strict';
	console.log('GridGame called');
	try 
	{
		/*var herotable = String(fs.readFileSync('public/HeroTable.html'));
		var orctable = String(fs.readFileSync('public/OrcTable.html'));
		var grid = String(fs.readFileSync('public/Grid.html'));
		var html = String(fs.readFileSync('public/GridGame.html'));
		*/
		
		if(request.session.firstname == null || request.session.firstname == undefined )
			request.session.firstname = 'Anonymous';
		
		var context = 
			{
				FirstName: request.session.firstname,
				HeroTable: herotable_html,
				OrcTable: orctable_html,
				Grid: grid_html
			}
		
		var result = replaceHtml( context, gridgame_html);
		
		response.writeHeader(200, 
			{
			"Content-Type" : "text/html"
			});
		response.end(result);
	} 
	catch (error) 
	{
		console.log(error);
		response.send(500,error.message);
	}

});


app.get('/TestScoring', function(request, response) 
{
	'use strict';
	console.log('TestScoring called');
	try {
		//var herotable = String(fs.readFileSync('public/HeroTable.html'));
		//var orctable = String(fs.readFileSync('public/OrcTable.html'));
		//var html = String(fs.readFileSync('public/TestScoring.html'));
		
		var context = 
			{
				HeroTable: herotable_html,
				OrcTable: orctable_html
			}
		
		var result = replaceHtml( context, testscoring_html);
		
		response.writeHeader(200, 
				{
					"Content-Type" : "text/html"
				});
		response.end(result);
	} 
	catch (error) 
	{
		console.log(error);
	}

});


app.get('/TestGrid', function(request, response) 
{
	'use strict';
	console.log('TestGrid called');
	try {
		//var grid = String(fs.readFileSync('public/Grid.html'));
		//var html = String(fs.readFileSync('public/TestGrid.html'));
		
		console.log(grid_html);
		console.log(testgrid_html);
		var context = 
			{
				Grid: grid_html
			}
		
		var result = replaceHtml( context, testgrid_html);
		
		response.writeHeader(200, 
			{
				"Content-Type" : "text/html"
			});
		response.end(result);
	} catch (error) {
		console.log(error);
	}

});

app.get('/readHero', function(request, response)
{
	console.log('readHero called.');

	// Asynch call
	var prog = nano.db.use(dbName);
	
    prog.get("hero", 
    		{ revs_info : true }, 
    	function(err, body) {
    			if (!err) 
    			{
    				console.log(body);
    				response.send(body);
    			} else 
    			{
	            var cscMessage = "No such record as: " + request.query.docName;
	            err.p282special = cscMessage;
	        	response.send(500, err);
    			}
        	
    });
});

app.get('/readOrc', function(request, response) {
	console.log('readOrc called.');

	// Asynch call
	var prog = nano.db.use(dbName);
    prog.get("orc", 
    		{ revs_info : true }, 
    	function(err, body) {
    			if (!err) 
    			{
    				console.log(body);
    				response.send(body);
    			} else 
    			{
	            var cscMessage = "No such record as: " + request.query.docName +
	                ". Use a the Get Doc Names button to find " +
	                "the name of an existing document."
	            err.p282special = cscMessage;
	        	response.send(500, err);
    			}
        	
    });
});


var readObjectfromCouchDB = function( docname, callback )
{
	
	var prog = nano.db.use(dbName);
    prog.get(docname, 
    		{ revs_info : true }, 
    		callback
    		);
        	
}


app.get('/readGrid', function(request, response) {
	console.log('ReadGrid called.');
	
	var prog = nano.db.use(dbName);
    prog.get("grid", 
    		{ revs_info : true }, 
    	function(err, body) {
    			if (!err) 
    			{
    				console.log(body);
    				response.send(body);
    			} else 
    			{
	            var cscMessage = "No such record as: " + request.query.docName +
	                ". Use a the Get Doc Names button to find " +
	                "the name of an existing document."
	            err.p282special = cscMessage;
	        	response.send(500, err);
    			}
        	
    });

});


app.get('/readNpcGrid', function(request, response) {
	console.log('ReadNpcGrid called.');

	var prog = nano.db.use(dbName);
    prog.get("npcgrid", 
    		{ revs_info : true }, 
    	function(err, body) {
    			if (!err) 
    			{
    				console.log(body);
    				response.send(body);
    			} else 
    			{
	            var cscMessage = "No such record as: " + request.query.docName +
	                ". Use a the Get Doc Names button to find " +
	                "the name of an existing document."
	            err.p282special = cscMessage;
	        	response.send(500, err);
    			}
        	
    });

});



var doInsert = function(response, data, docName) {'use strict';
	var prog = nano.db.use(dbName);
	prog.insert(data, docName, function(err, body) 
			{
				console.log('In sendToCouch callback');
				if (!err) 
				{
					response.send({ "result" : "success" });
					console.log('It saved!');
					return;
				} 
				else 
				{
					response.send(500, err);
					return;
				}
			}
	);
};


app.get('/writeHero', function(request, response) {
	console.log('writeHero called: ' + request.query);
	
	var docName = "hero";
	
	var person = request.query;
	var personString = JSON.stringify(person, null, 4);
	
	var prog = nano.db.use(dbName);
    prog.get(docName, function(error, existing) 
		{
	        if(!error) { 
	            console.log("Document exists. Doing Update.");
	            person._rev = existing._rev;
	            doInsert(response, person, docName);
	        }  else {
	            console.log("Document does not exist. Doing insert.");
	            doInsert(response, person, docName);
	        }
		});
    console.log('It\'s saved!');
});


app.get('/writeOrc', function(request, response) {
	console.log('writeOrc called: ' + request.query);
	
	var docName = "orc";
	
	var person = request.query;
	var personString = JSON.stringify(person, null, 4);
	
	var prog = nano.db.use(dbName);
    prog.get(docName, function(error, existing) 
    		{
		        if(!error) { 
		            console.log("Document exists. Doing Update.");
		            person._rev = existing._rev;
		            doInsert(response, person, docName);
		        }  else {
		            console.log("Document does not exist. Doing insert.");
		            doInsert(response, person, docName);
		        }
    		});
    
});





app.post('/writeGrid', function(request, response) 
{
	console.log('writeGrid called: ' + request.body);
	
	var grid = request.body;
	console.log(grid)
	var gridString = JSON.stringify(grid, null, 4);
	console.log(gridString);
	
	var docName = "grid";
	
	var prog = nano.db.use(dbName);
    prog.get(docName, function(error, existing) 
    		{
		        if(!error) { 
		            console.log("Document exists. Doing Update.");
		            grid._rev = existing._rev;
		            doInsert(response, grid, docName);
		        }  else {
		            console.log("Document does not exist. Doing insert.");
		            doInsert(response, grid, docName);
		        }
    		});

	response.send('{"result":"success"}');
});



app.post('/writeNpcGrid', function(request, response) 
{
	console.log('writeNpcGrid called: ' + request.body);
	
	var grid = request.body;
	var gridString = JSON.stringify(grid, null, 4);
	
	var docName = "npcgrid";
	
	var prog = nano.db.use(dbName);
    prog.get(docName, function(error, existing) 
    		{
		        if(!error) { 
		            console.log("Document exists. Doing Update.");
		            grid._rev = existing._rev;
		            doInsert(response, grid, docName);
		        }  else {
		            console.log("Document does not exist. Doing insert.");
		            doInsert(response, grid, docName);
		        }
    		});

	response.send('{"result":"success"}');
});



app.get('/authenticate', function(req, res) {
	console.log("Authenticate called");
	
	// User supplied identifier
	console.log(req.query);
	req.session.firstname = req.query.firstname;
	var query = querystring.parse(req.query);
	console.log("Parsed Query: " + query);
	var identifier = req.query.openid_identifier;
	console.log("identifier: " + identifier);

	// Resolve identifier, associate, and build authentication URL
	relyingParty.authenticate(identifier, false, function(error, authUrl) {
		if (error) {
			res.writeHead(200);
			res.end('Authentication failed: ' + error.message);
		} else if (!authUrl) {
			res.writeHead(200);
			res.end('Authentication failed');
		} else {
			res.writeHead(302, {
				Location : authUrl
			});
			res.end();
		}
	});
});


function replaceHtml( context, html) {
	// This code uses Handlebars to put 'context'
	// into the 'html' variable text.
    'use strict';
   
    var template=handlebars.compile(html);
    var result = template(context);
 
    return result;
}

app.use("/public", express.static(__dirname + '/public'));
app.use("/css", express.static(__dirname + '/css'));
app.use("/images", express.static(__dirname + '/images'));
app.use("/sound", express.static(__dirname + '/sound'));

app.use(app.router);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
//app.use(app.router);
app.use( require( 'stylus' ).middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/Page01', routes.page01);
app.get('/Page02', routes.page02);
app.get('/Page03', routes.page03);
app.get('/Page04', routes.page04);
app.get('/Story', routes.story);


app.listen(port);
console.log('Listening on port :' + port);
