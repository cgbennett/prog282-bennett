
/*
 * GET home page.
 */

exports.index = function(req, res){
	res.render('index', { title: 'Prog 282 - bennett',
  						name: 'Chris Bennett' });
};

exports.page01 = function(req, res){
	res.render('page01', { title: 'Page01',
  						name: 'Mr Wizard' });
};

exports.page02 = function(req, res){
	  res.render('page02', { title: 'Page02',
	  						name: 'Mr Happy' });
};
	
exports.page03 = function(req, res){
	res.render('page03', { title: 'Page03',
		  						name: 'Mr Sad' });
};
		
exports.page04 = function(req, res){
	res.render('page04', { title: 'Page04',
			  						name: 'Mr Angry' });
};


exports.gridgame = function(req, res){
	res.render('http://54.243.217.141:30025/GridGame', { title: 'GridGame',
			  						name: 'Mr Sweet' });
};



exports.story = function(req, res){
	res.render('story', { title: 'GridGame Back Story',
			  						author: 'Christopher Bennett' });
};
