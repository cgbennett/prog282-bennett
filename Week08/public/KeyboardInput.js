 
ELF.own.KeyboardInput = (function() 
{
	var moveAudioFilename = "sound/move.wav";
	
	var audiolist_index = 0;
	var AUDIO_MAX = 20;
	
	var audiolist = new Array();
	for(var i=0; i<AUDIO_MAX; i++ )
	{
		audiolist[i] = new Audio(moveAudioFilename);
	}
	
	
	
	/*
	audiolist[0] = new Audio(moveAudioFilename);
	audiolist[1] = new Audio(moveAudioFilename);
	audiolist[2] = new Audio(moveAudioFilename);
	audiolist[3] = new Audio(moveAudioFilename);
	audiolist[4] = new Audio(moveAudioFilename);
	audiolist[5] = new Audio(moveAudioFilename);
	audiolist[6] = new Audio(moveAudioFilename);
	audiolist[7] = new Audio(moveAudioFilename);
	audiolist[8] = new Audio(moveAudioFilename);
	audiolist[9] = new Audio(moveAudioFilename);
	audiolist[10] = new Audio(moveAudioFilename);
	audiolist[11] = new Audio(moveAudioFilename);
	audiolist[12] = new Audio(moveAudioFilename);
	audiolist[13] = new Audio(moveAudioFilename);
	audiolist[14] = new Audio(moveAudioFilename);
	audiolist[15] = new Audio(moveAudioFilename);
	audiolist[16] = new Audio(moveAudioFilename);
	audiolist[17] = new Audio(moveAudioFilename);
	audiolist[18] = new Audio(moveAudioFilename);
	audiolist[19] = new Audio(moveAudioFilename);
*/
	
	
	function KeyboardInput()
	{
	}

	KeyboardInput.prototype.doKeyDown = function(evt) 
	{
		var grid = ELF.run.grid.getGrid();
		
		// Test whether some variable have loaded yet.
		if( grid == null )
			return;
		if( ELF.run.hero == null || ELF.run.hero == undefined )
			return;
		
		// If we are in combat then don't allow movement keystrokes.
		if( $('#divCombatCanvas').is(":visible") )
			return;
		
    	//var moveSound = new Audio(moveAudioFilename);
		
		var moveAudio = audiolist[audiolist_index++%AUDIO_MAX];
		moveAudio.play();
		
        switch (evt.keyCode) 
        {
            case 38:
                /* Up arrow was pressed */
            	if(ELF.run.hero.y>0 && grid[ELF.run.hero.x][ELF.run.hero.y-1]>0)
            		{
            			ELF.run.hero.y--;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            			//ELF.run.hero.y_offset--;
            		}
                break;
            case 40:
                /* Down arrow was pressed */
            	if(ELF.run.hero.y<grid.length-1 && grid[ELF.run.hero.x][ELF.run.hero.y+1]>0)
            		{
            			ELF.run.hero.y++;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            			//ELF.run.hero.y_offset++;
            		}
                break;
            case 37:
                /* Left arrow was pressed */
            	if(ELF.run.hero.x>0 && grid[ELF.run.hero.x-1][ELF.run.hero.y]>0)
            		{
            			ELF.run.hero.x--;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            			//ELF.run.hero.x_offset--;
            		}
                break;
            case 39:
                /* Right arrow was pressed */
            	if(ELF.run.hero.x<grid[0].length-1 && grid[ELF.run.hero.x+1][ELF.run.hero.y]>0)
            		{
            			ELF.run.hero.x++;
            			ELF.run.stats.UpdateStats();
            			ELF.run.grid.DetectCollision(ELF.run.hero.x, ELF.run.hero.y);
            			//ELF.run.hero.x_offset++;
            		}
                break;
        }
        
        ELF.run.grid.hasReachedGoal();
	}
	

	
	KeyboardInput.prototype.UnitTests = function()
	{

	}
	
	
	return KeyboardInput;
})();

