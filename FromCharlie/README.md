Notes from Charlie
==================

Week08

This is very good, Chris.

One problem I see is that you are letting global variables creep in and 
pollute the global name space. At the bottom of **Grids.js** you have
these four functions in the global namespace:

~~~~
var ShowGrid = function()
{
	if( $('#divMainCanvas').is(":visible") )
		$('#divMainCanvas').hide('Slide');
	else 
		$('#divMainCanvas').show('Slide');
}

var ShowCombat = function()
{
	if( $('#divCombatCanvas').is(":visible") )
		$('#divCombatCanvas').hide('Slide');
	else
		$('#divCombatCanvas').show('Slide');
}

var ShowCharAttributes = function()
{
	if( $('#divAttributes').is(":visible") )
		$('#divAttributes').hide('Slide');
	else
		$('#divAttributes').show('Slide');
}
~~~~

I have similar code, and placed them in a file called
**Display.js**, and I believe I made that object one of
those that is global to the application: **ELF.run.display**.
That way all the code in my program could call the 
display functions when needed. The key points:

* Don't pollute the global name space
* There are easy ways to to create "global" methods
in a real (ie module pattern) object that are not in the 
global namespaces.

Another issue I see with your code is the shere size 
of Grid.js. To me, methods like **DoCombat** and 
**MakeRandomGrids** don't belong in the same
object as a **draw** or **getCanvas** method. In fact,
I see three objects here:

* Draw.js
* Grids.js
* Combat.js

We just didn't have time to discuss object design in much
depth in this course. It is, of course, a controversial subject
and there is no one right answer to most design questions.
Yet there are some guidelines that can be agreed upon
by most people.

Once again, great job. This is excellent. 

Do you want to show your program to the class on Monday?

Week06 - Midterm
------------------------

Overall, this is good. You have learned the basic skills we 
covered so far in this course.

I needed to change one line of code to get this to work. On line 88
of Stats.js you call ELF.run.stats.showNpcAttributes, which is a private
method. That call failed, and I had to change it to a shoNpcAttributes
to get it to work:

~~~~
	var readOrcJson = function(npc_id) 
	{
		$.getJSON('/readOrc', function( charAttributes ) 
		{
			ELF.run.orc = convertToInts(charAttributes);
			//showHeroAttributes(); 
		})
		.success(function() 
		{ 
			showDebug('success'); 
			showNpcAttributes();                            // Charlie added this.
			// ELF.run.stats.showNpcAttributes();  // Did not work!
		})
		.error(function(jqXHR, textStatus, errorThrown) 
		{ 
			alert("error calling JSON. Try JSONLint or JSLint: " + textStatus); 
		})
		.complete(function() { console.log("csc: completed call to get index.json"); });
	}
~~~~

Three things you could have done to make this better:

1)  More modules. Compat and Drawing are in the same module. They are different 
tasks. CalcStrike is a method in Stats, and that seems like it is part of the combat
logic.
2) Read and write state. As far as I can tell, you never actually call your writeJson
method. Reading in the grid, saving game state or stats, all would be good
exercises.
3) Add more test cases. This is fairly minimal unit testing.


Week05 - CanvasGrid
-----------------------------

You should be careful about hardcoding "magic numbers" over and over. You write:

~~~~
combat_context.drawImage(imagesPalette, rectSize, 0, 
				rectSize, rectSize,
				0, 0, 
				300, 500);
~~~~

I know what 300 and 500 are, but others won't. A name like bitmapWidth or bitmapHeight 
would help. Also, you are putting the same numbers in multiple places, which is 
always a sign that you need to declare a variable.

We all get careless about this sort of thing, but it is probably better form to always
use curly braces when setting up a code block. You wrote:


	if( npc[x][y]>0 )
		DoCombat(npc[x][y]);

It should be:


		if( npc[x][y]>0 ) {
			DoCombat(npc[x][y]);			
		}

The following are from Grid.js:

~~~~
var ShowGrid = function()
{
	if( $('#divMainCanvas').is(":visible") )
		$('#divMainCanvas').hide('Slide');
	else 
		$('#divMainCanvas').show('Slide');
}

var ShowCombat = function()
{
	if( $('#divCombatCanvas').is(":visible") )
		$('#divCombatCanvas').hide('Slide');
	else
		$('#divCombatCanvas').show('Slide');
}

var ShowCharAttributes = function()
{
	if( $('#divAttributes').is(":visible") )
		$('#divAttributes').hide('Slide');
	else
		$('#divAttributes').show('Slide');
}
~~~~

They should probably be in a separate object/file with a name
like Display.js. They control not the grid, per se, but the layout
of the game. So I guess Layout.js would be good also.

Overall, great job.

Week 03-CanvasGrid
------------------

Take a look here, and change the name in your .project file
based on what you read about **.project files**:

<http://localhost:20212/charlie/development/android/Eclipse.html#createProject>

Take a look at this method, quoted directly from your program:

~~~~
app.get('/go', function(request, res) {
	console.log("In Go Callback");
	var query = request.query;
	console.log(query);
	console.log(query['openid.claimed_id']);
	relyingParty.verifyAssertion(request, function(error, result) {
		var isGood = !error && result.authenticated ? '<h1>Success!</h1>'
				: '<h1>Failed</h1>'
		res.writeHead(200, {
			'Content-Type' : 'text/html'
		});
		res.write(isGood);
		res.write('<p>' + query['openid.assoc_handle'] + '</p>');
		res.write('<p>' + query['openid.claimed_id'] + '</p>');
		res.write('<p>' + query['openid.identity'] + '</p>');
		res.write('<p>' + query['openid.mode'] + '</p>');
		res.write('<p>' + query['openid.ns'] + '</p>');
		res.write('<p>' + query['openid.op_endpoint'] + '</p>');
		res.write('<p>' + query['openid.response_nonce'] + '</p>');
		res.write('<p>' + query['openid.return_to'] + '</p>');
		res.write('<p>' + query['openid.sig'] + '</p>');
		res.write('<p>' + query['openid.signed'] + '</p>');
		res.end('bye');
	});
});
~~~~

This is the method that you need to change. In particular, you want to add code 
after this line that will allow you to launch your game:

	relyingParty.verifyAssertion(request, function(error, result) {
	
The code you insert will look something like the code from this method, which
again I am taking verbatim from your current version of GameGrid:

~~~~
app.get('/launchSecondHtmlFile', function(request, response) {
	console.log('launchSecondHtmlFile called');
	try {
		var html = fs.readFileSync('Public/SecondFile.html');
		response.writeHeader(200, {
			"Content-Type" : "text/html"
		});
		response.end(html);
	} catch (error) {
		console.log(error);
	}

});
~~~~

I'm not sure what you are asking about the OpenId issue. The code you have
turned in works fine. I can use it to sign into OpenId and call your current
"go" route handler. The problem is simply that it launches the code from
my sample program, and not the code for your game.

Please try again and resubmit.